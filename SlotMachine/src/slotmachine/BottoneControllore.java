/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.scene.control.Button;

/**
 *
 * @author giovanni
 */
public class BottoneControllore extends Button {

    String text;

    public BottoneControllore(String text) {
        this.text = text;
        this.setText(this.text);
        this.setMaxSize(60, 60);
        this.setMinSize(60, 60);
    }
}
