/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.scene.control.Label;
import javafx.scene.text.Font;

/**
 *
 * @author giovanni
 */
public class Contatore extends Label {

    String text;
    int value;

    public Contatore(String text) {
        this.text = text;
        this.value = 0;
        this.setText(text);
        this.setFont(new Font(20));
    }
}
