/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.geometry.Pos;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 *
 * @author giovanni
 */
public class Ruota extends StackPane {

    Shape shape;
    int numero;

    public Ruota() {
        this.setMaxWidth(110);
        this.setMinWidth(110);
        shape = determineShape();
        this.getChildren().add(shape);
        this.setAlignment(Pos.CENTER);
    }

    public Shape determineShape() {
        this.numero = (int) (Math.random() * 6);
        switch (this.numero) {
            case 0:
                Circle circle = new Circle(50, 50, 50);
                circle.setFill(Color.RED);
                circle.setStroke(Color.BLACK);
                circle.setEffect(new InnerShadow(30, Color.DARKBLUE));
                return circle;
            case 1:
                Polygon hexagon = new Polygon(50, 0, 0, 30, 0, 70, 50, 100, 100, 70, 100, 30);
                hexagon.setFill(Color.YELLOW);
                hexagon.setStroke(Color.BLACK);
                hexagon.setEffect(new InnerShadow(30, Color.CORAL));
                return hexagon;
            case 2:
                Line line = new Line(0, 100, 100, 0);
                line.setFill(Color.BLACK);
                line.setStroke(Color.BLACK);
                return line;
            case 3:
                Rectangle square = new Rectangle(100, 100);
                square.setFill(Color.BLUE);
                square.setStroke(Color.BLACK);
                square.setEffect(new InnerShadow(50, Color.AQUAMARINE));
                return square;
            case 4:
                Rectangle rombo = new Rectangle(70, 70);
                rombo.setRotate(45);
                rombo.setFill(Color.GREEN);
                rombo.setStroke(Color.BLACK);
                rombo.setEffect(new InnerShadow(50, Color.DARKGREEN));
                return rombo;
            case 5:
                Polygon triangle = new Polygon(50, 0, 0, 100, 100, 100);
                triangle.setFill(Color.FUCHSIA);
                triangle.setStroke(Color.BLACK);
                triangle.setEffect(new InnerShadow(50, Color.BLUEVIOLET));
                return triangle;
        }
        return null;
    }

    public boolean equals(Ruota ruota) {
        return this.numero == ruota.numero;
    }
}
