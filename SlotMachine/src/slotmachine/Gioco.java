/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author giovanni
 */
public class Gioco {

    int i;
    Scene scene;
    Stage primaryStage;
    Pannello center;
    Label topbar;
    VBox bottombar;

    HBox counters;
    Contatore credito, punteggio;

    HBox buttons;
    BottoneControllore reset, spin, pay;

    VBox body;
    HBox root;

    Cash right;

    Stage winStage;
    Stage payStage;
    Stage notEnoughCreditStage;

    public Gioco() {
        center = new Pannello();
        bottombar = new VBox();
        counters = new HBox();
        buttons = new HBox();
        body = new VBox();
        body.setPrefSize(400, 400);
        root = new HBox();
        right = new Cash();
        topbar = new Label("SUPER SLOTS");
        credito = new Contatore("Credito: 0");
        punteggio = new Contatore("Punteggio: 0");
        reset = new BottoneControllore("RESET");
        reset.addEventHandler(ActionEvent.ACTION, new ResetListener());
        spin = new BottoneControllore("SPIN");
        spin.setDisable(true);
        spin.addEventHandler(ActionEvent.ACTION, new SpinListener());
        pay = new BottoneControllore("PAY");
        pay.setDisable(true);
        pay.addEventHandler(ActionEvent.ACTION, new PayListener());
        topbar.setAlignment(Pos.CENTER);
        topbar.setFont(new Font(50));

        counters.setSpacing(30);
        counters.setPadding(new Insets(20, 20, 20, 20));
        counters.setAlignment(Pos.CENTER);
        counters.getChildren().addAll(credito, punteggio);

        buttons.setSpacing(30);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(reset, spin, pay);

        bottombar.setPadding(new Insets(20, 20, 20, 20));
        bottombar.setSpacing(50);
        bottombar.getChildren().addAll(counters, buttons);
        bottombar.setAlignment(Pos.CENTER);

        center.addEventHandler(MouseEvent.MOUSE_CLICKED, new ListenerPannello());

        body.getChildren().addAll(topbar, center, bottombar);
        right.setAlignment(Pos.CENTER);
        right.addEventHandler(MouseEvent.MOUSE_CLICKED, new ListenerCash());

        root.setSpacing(50);
        root.getChildren().addAll(body, right);

        scene = new Scene(root, 600, 400);
        primaryStage = new Stage();
        primaryStage.setTitle("SUPER SLOTS");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    class ListenerPannello implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {
            if (punteggio.value > 0) {
                for (int i = 0; i < 3; i++) {
                    //detects which of the three panes have been clicked
                    if (center.simboli[i].getChildren().contains(event.getTarget()) || event.getTarget().equals(center.simboli[i])) {
                        center.simboli[i].getChildren().clear();
                        center.simboli[i] = new Ruota();
                        center.add(center.simboli[i], i, 0);
                        center.setMargin(center.simboli[i], new Insets(10, 10, 10, 10));
                        checkVittoria();
                    }
                }
                setPoints(punteggio.value / 2);
            }

        }
    }

    class ListenerCash implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {
            if (event.getTarget() instanceof Text || event.getTarget() instanceof Circle) {
                if (right.numCoins > 0) {
                    right.getChildren().remove(right.numCoins - 1);
                    right.numCoins--;
                    setCredit(credito.value + 100);
                }
            }
        }

    }

    class PayListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            pay();
        }

    }

    class WinListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            winStage.close();
        }

    }

    class SpinListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            spin();
        }

    }

    public void spin() {
        if (punteggio.value > 0) {
            body.getChildren().clear();
            center = new Pannello();
            center.addEventHandler(MouseEvent.MOUSE_CLICKED, new ListenerPannello());
            body.getChildren().addAll(topbar, center, bottombar);
            setPoints(punteggio.value / 2);
            checkVittoria();
        }
    }

    class CreditListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            notEnoughCreditStage.close();
        }

    }

    class ResetListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            if (credito.value < 100) {
                notEnoughCredit();
            } else {
                setCredit(credito.value - 100);
                setPoints(128);
                spin();
                setPoints(128);
            }
        }

    }

    class PayButtonListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            payStage.close();
            primaryStage.close();
            new Gioco();
        }

    }

    public void checkVittoria() {
        Boolean vittoria = true;
        for (int i = 0; i < 2; i++) {
            if (!center.simboli[i].equals(center.simboli[i + 1])) {
                vittoria = false;
            }
        }
        if (vittoria) {
            vittoria();
        }
    }

    public void notEnoughCredit() {
        notEnoughCreditStage = new Stage();
        VBox text = new VBox();
        text.setSpacing(50);
        text.setAlignment(Pos.CENTER);
        Text haiVinto = new Text("Credito insufficiente!");

        Button okay = new Button("Ok");
        okay.addEventHandler(ActionEvent.ACTION, new CreditListener());
        Scene scene = new Scene(text, 200, 200);
        text.getChildren().add(haiVinto);
        text.getChildren().add(okay);
        notEnoughCreditStage.setTitle("Non hai abbastanza credito!");
        notEnoughCreditStage.setScene(scene);
        notEnoughCreditStage.showAndWait();
    }

    public void vittoria() {
        winStage = new Stage();
        VBox text = new VBox();
        text.setSpacing(50);
        text.setAlignment(Pos.CENTER);
        Text haiVinto = new Text("Complimenti, hai vinto!");

        Button okay = new Button("Ok");
        okay.addEventHandler(ActionEvent.ACTION, new WinListener());
        Scene scene = new Scene(text, 200, 200);
        text.getChildren().add(haiVinto);
        text.getChildren().add(okay);
        winStage.setTitle("Complimenti, hai vinto!");
        winStage.setScene(scene);
        winStage.showAndWait();
        this.setCredit(this.punteggio.value * 100 + this.credito.value);
        this.setPoints(0);
    }

    public void pay() {
        payStage = new Stage();
        VBox text = new VBox();
        text.setSpacing(50);
        text.setAlignment(Pos.CENTER);
        Text haiVinto = new Text("Hai vinto " + this.credito.value / 100 + " euro!");

        Button okay = new Button("Ok");
        okay.addEventHandler(ActionEvent.ACTION, new PayButtonListener());
        Scene scene = new Scene(text, 200, 200);
        text.getChildren().add(haiVinto);
        text.getChildren().add(okay);
        payStage.setTitle("Complimenti, hai vinto!");
        payStage.setScene(scene);
        payStage.showAndWait();
        this.setCredit(this.punteggio.value * 100 + this.credito.value);
        this.setPoints(0);
    }

    public void setCredit(int credit) {
        this.credito.value = credit;
        this.credito.text = "Credito: " + credit;
        this.credito.setText(this.credito.text);
        if (this.credito.value > 0) {
            this.spin.setDisable(false);
            this.pay.setDisable(false);
        }
    }

    public void setPoints(int points) {
        this.punteggio.value = points;
        this.punteggio.text = "Punteggio: " + points;
        this.punteggio.setText(this.punteggio.text);
    }
}
