/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 *
 * @author giovanni
 */
public class Cash extends StackPane {

    StackPane[] coins;
    int numCoins;

    public Cash(int numCoins) {
        this.numCoins = numCoins;
        this.setMinSize(100, 600);
        this.setMaxSize(100, 600);
        this.coins = new StackPane[numCoins];
        for (int i = 0; i < numCoins; i++) {
            coins[i] = new StackPane();
            coins[i].setMinSize(50, 50);
            coins[i].setMaxSize(50, 50);
            Circle inside = new Circle(i * 20, i * 20, 40);
            inside.setFill(Color.GREEN);
            inside.setStroke(Color.BLACK);
            Text euro1 = new Text("1€");
            coins[i].getChildren().add(inside);
            coins[i].getChildren().add(euro1);
            coins[i].setLayoutX(i * 20);
            coins[i].setLayoutY(i * 20);
            coins[i].setTranslateX(i * 20);
            coins[i].setTranslateY(i * 20);
            this.getChildren().add(coins[i]);
        }
    }

    public Cash() {
        this(3);
    }
}
