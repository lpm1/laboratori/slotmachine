/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotmachine;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

/**
 *
 * @author giovanni
 */
public class Pannello extends GridPane {

    Ruota[] simboli;

    public Pannello() {
        this.setGridLinesVisible(true);
        this.setMaxWidth(330);
        this.setMinWidth(330);
        this.setMaxHeight(110);
        this.setMinHeight(110);
        this.setPadding(new Insets(10, 10, 10, 10));
        this.simboli = new Ruota[3];
        for (int i = 0; i < 3; i++) {
            this.simboli[i] = new Ruota();
            this.add(this.simboli[i], i, 0);
            this.setMargin(this.simboli[i], new Insets(10, 10, 10, 10));
        }
    }
}
